<?php

namespace ContainerXpUmrNW;

use Symfony\Component\DependencyInjection\Argument\RewindableGenerator;
use Symfony\Component\DependencyInjection\Exception\RuntimeException;

/**
 * @internal This class has been auto-generated by the Symfony Dependency Injection Component.
 */
class get_ServiceLocator_J7KCKHoService extends App_KernelDevDebugContainer
{
    /**
     * Gets the private '.service_locator.j7KCKHo' shared service.
     *
     * @return \Symfony\Component\DependencyInjection\ServiceLocator
     */
    public static function do($container, $lazyLoad = true)
    {
        return $container->privates['.service_locator.j7KCKHo'] = new \Symfony\Component\DependencyInjection\Argument\ServiceLocator($container->getService, [
            'em' => ['services', 'doctrine.orm.default_entity_manager', 'getDoctrine_Orm_DefaultEntityManagerService', false],
            'offre' => ['privates', '.errored..service_locator.j7KCKHo.App\\Entity\\Offre', NULL, 'Cannot autowire service ".service_locator.j7KCKHo": it references class "App\\Entity\\Offre" but no such service exists.'],
        ], [
            'em' => '?',
            'offre' => 'App\\Entity\\Offre',
        ]);
    }
}
