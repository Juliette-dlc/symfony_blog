<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog/article.html.twig */
class __TwigTemplate_a49c17ec324b3a8726ba2b3b9cff710e0e104bd5bc086faada629d60eb9a64d8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/article.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/article.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "blog/article.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <section class=\"sectionArticle\">
            ";
        // line 5
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["offre"]);
        foreach ($context['_seq'] as $context["_key"] => $context["offre"]) {
            // line 6
            echo "            <a href=\"/article/";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "id", [], "any", false, false, false, 6), "html", null, true);
            echo "\"
                <div class=\"sectionArticle__container\">
                    <div class=\"sectionArticle__container--first\">
                        <div class=\"sectionArticle__container--first--left\">
                            <p class=\"sectionArticle__container--first--left--contrat\">";
            // line 10
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "contrat", [], "any", false, false, false, 10), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "typecontrat", [], "any", false, false, false, 10), "html", null, true);
            echo "</p>
                            <img class=\"sectionArticle__container--first--left--img\" src=\"/build/images/perso_icon.png\" alt=\"une icone de personne\">
                        </div>
                        <div class=\"sectionArticle__container--first--right\">
                        </div>
                    </div>
                    <div class=\"sectionArticle__container--second\">
                        <div class=\"sectionArticle__container--second--right\">
                            <p class=\"sectionArticle__container--second--right--title\">";
            // line 18
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "title", [], "any", false, false, false, 18), "html", null, true);
            echo "</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Créé le : ";
            // line 19
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "dateCreation", [], "any", false, false, false, 19), "d/m/Y"), "html", null, true);
            echo "</p>
                            <p class=\"sectionArticle__container--second--right--adresse\">";
            // line 20
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "adresse", [], "any", false, false, false, 20), "html", null, true);
            echo ", ";
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "codePostal", [], "any", false, false, false, 20), "html", null, true);
            echo "</p>
                        </div>
                        <div class=\"sectionArticle__container--second--left\">
                            <p class=\"sectionArticle__container--second--left--description\">";
            // line 23
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["offre"], "description", [], "any", false, false, false, 23), "html", null, true);
            echo "</p>
                        </div>
                    </div>
                </div>
            </a>
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['offre'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 29
        echo "    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog/article.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  124 => 29,  112 => 23,  104 => 20,  100 => 19,  96 => 18,  83 => 10,  75 => 6,  71 => 5,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <section class=\"sectionArticle\">
            {% for offre in offre %}
            <a href=\"/article/{{offre.id}}\"
                <div class=\"sectionArticle__container\">
                    <div class=\"sectionArticle__container--first\">
                        <div class=\"sectionArticle__container--first--left\">
                            <p class=\"sectionArticle__container--first--left--contrat\">{{offre.contrat}}, {{offre.typecontrat}}</p>
                            <img class=\"sectionArticle__container--first--left--img\" src=\"/build/images/perso_icon.png\" alt=\"une icone de personne\">
                        </div>
                        <div class=\"sectionArticle__container--first--right\">
                        </div>
                    </div>
                    <div class=\"sectionArticle__container--second\">
                        <div class=\"sectionArticle__container--second--right\">
                            <p class=\"sectionArticle__container--second--right--title\">{{offre.title}}</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Créé le : {{offre.dateCreation |date(\"d/m/Y\")}}</p>
                            <p class=\"sectionArticle__container--second--right--adresse\">{{offre.adresse}}, {{offre.codePostal}}</p>
                        </div>
                        <div class=\"sectionArticle__container--second--left\">
                            <p class=\"sectionArticle__container--second--left--description\">{{offre.description}}</p>
                        </div>
                    </div>
                </div>
            </a>
            {% endfor %}
    </section>
{% endblock %}
", "blog/article.html.twig", "C:\\Users\\Juliettedlc\\Documents\\Ynov\\symfony_blog\\blog\\templates\\blog\\article.html.twig");
    }
}
