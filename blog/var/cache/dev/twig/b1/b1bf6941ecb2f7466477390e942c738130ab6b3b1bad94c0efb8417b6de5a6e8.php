<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* blog/offre.html.twig */
class __TwigTemplate_902210f9b947f6f059bc16bd86941b0933e41a5cdd3cd77a4ff46ad65f3564a8 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/offre.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "blog/offre.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "blog/offre.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <section class=\"sectionArticle\">
                <div class=\"sectionArticle__container\">
                    <div class=\"sectionArticle__container--first\">
                        <div class=\"sectionArticle__container--first--left\">
                            <p class=\"sectionArticle__container--first--left--contrat\">";
        // line 8
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 8, $this->source); })()), "contrat", [], "any", false, false, false, 8), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 8, $this->source); })()), "typecontrat", [], "any", false, false, false, 8), "html", null, true);
        echo "</p>
                            <img class=\"sectionArticle__container--first--left--img\" src=\"/build/images/perso_icon.png\" alt=\"une icone de personne\">
                        </div>
                        <div class=\"sectionArticle__container--first--right\">
                        </div>
                    </div>
                    <div class=\"sectionArticle__container--second\">
                        <div class=\"sectionArticle__container--second--right\">
                            <p class=\"sectionArticle__container--second--right--title\">";
        // line 16
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 16, $this->source); })()), "title", [], "any", false, false, false, 16), "html", null, true);
        echo "</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Créé le : ";
        // line 17
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 17, $this->source); })()), "dateCreation", [], "any", false, false, false, 17), "d/m/Y"), "html", null, true);
        echo "</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Modifié le : ";
        // line 18
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 18, $this->source); })()), "dateUpdate", [], "any", false, false, false, 18), "d/m/Y"), "html", null, true);
        echo "</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Fin de contrat (si CDD ou Free) : ";
        // line 19
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 19, $this->source); })()), "finMission", [], "any", false, false, false, 19), "d/m/Y"), "html", null, true);
        echo "</p>
                            <p class=\"sectionArticle__container--second--right--adresse\">";
        // line 20
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 20, $this->source); })()), "adresse", [], "any", false, false, false, 20), "html", null, true);
        echo ", ";
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 20, $this->source); })()), "codePostal", [], "any", false, false, false, 20), "html", null, true);
        echo "</p>
                        </div>
                        <div class=\"sectionArticle__container--second--left\">
                            <p class=\"sectionArticle__container--second--left--description\">";
        // line 23
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["offres"]) || array_key_exists("offres", $context) ? $context["offres"] : (function () { throw new RuntimeError('Variable "offres" does not exist.', 23, $this->source); })()), "description", [], "any", false, false, false, 23), "html", null, true);
        echo "</p>
                        </div>
                    </div>
                </div>
    </section>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "blog/offre.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  111 => 23,  103 => 20,  99 => 19,  95 => 18,  91 => 17,  87 => 16,  74 => 8,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}
    <section class=\"sectionArticle\">
                <div class=\"sectionArticle__container\">
                    <div class=\"sectionArticle__container--first\">
                        <div class=\"sectionArticle__container--first--left\">
                            <p class=\"sectionArticle__container--first--left--contrat\">{{offres.contrat}}, {{offres.typecontrat}}</p>
                            <img class=\"sectionArticle__container--first--left--img\" src=\"/build/images/perso_icon.png\" alt=\"une icone de personne\">
                        </div>
                        <div class=\"sectionArticle__container--first--right\">
                        </div>
                    </div>
                    <div class=\"sectionArticle__container--second\">
                        <div class=\"sectionArticle__container--second--right\">
                            <p class=\"sectionArticle__container--second--right--title\">{{offres.title}}</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Créé le : {{offres.dateCreation |date(\"d/m/Y\")}}</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Modifié le : {{offres.dateUpdate |date(\"d/m/Y\")}}</p>
                            <p class=\"sectionArticle__container--second--right--dateCreation\">Fin de contrat (si CDD ou Free) : {{offres.finMission |date(\"d/m/Y\")}}</p>
                            <p class=\"sectionArticle__container--second--right--adresse\">{{offres.adresse}}, {{offres.codePostal}}</p>
                        </div>
                        <div class=\"sectionArticle__container--second--left\">
                            <p class=\"sectionArticle__container--second--left--description\">{{offres.description}}</p>
                        </div>
                    </div>
                </div>
    </section>
{% endblock %}
", "blog/offre.html.twig", "C:\\Users\\Juliettedlc\\Documents\\Ynov\\symfony_blog\\blog\\templates\\blog\\offre.html.twig");
    }
}
