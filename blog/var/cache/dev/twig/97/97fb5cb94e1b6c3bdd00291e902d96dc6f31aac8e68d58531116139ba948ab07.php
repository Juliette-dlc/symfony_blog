<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/modifier.html.twig */
class __TwigTemplate_d9c30be82aba3506477ac089d013e03b86697a0b4a3e02e12b1d166f89fa720d extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/modifier.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/modifier.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security/modifier.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "
    ";
        // line 5
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 5, $this->source); })()), "user", [], "any", false, false, false, 5)) {
            // line 6
            echo "    <section class=\"sectionForm\">
        <div class=\"sectionForm--container\">
            ";
            // line 8
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 8, $this->source); })()), 'form_start');
            echo "
                <div class=\"sectionForm__form--container\">
                    ";
            // line 10
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 10, $this->source); })()), "Title", [], "any", false, false, false, 10), 'row');
            echo "
                </div>
                <div class=\"sectionForm__form--container\">
                    ";
            // line 13
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 13, $this->source); })()), "contrat", [], "any", false, false, false, 13), 'row');
            echo "
                </div>
                <div class=\"sectionForm__form--container\">
                    ";
            // line 16
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 16, $this->source); })()), "typeContrat", [], "any", false, false, false, 16), 'row');
            echo "
                </div>
                <div class=\"sectionForm__form--container\">
                    ";
            // line 19
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 19, $this->source); })()), "fin_mission", [], "any", false, false, false, 19), 'row');
            echo "
                </div>
                <div class=\"sectionForm__form--container\">
                    ";
            // line 22
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 22, $this->source); })()), "Description", [], "any", false, false, false, 22), 'row');
            echo "
                </div>
                <div class=\"sectionForm__form--container\">
                    ";
            // line 25
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 25, $this->source); })()), "Adresse", [], "any", false, false, false, 25), 'row');
            echo "
                </div>
                <div class=\"sectionForm__form--container\">
                    ";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 28, $this->source); })()), "Code_postal", [], "any", false, false, false, 28), 'row');
            echo "
                </div>
                <div class=\"sectionForm__form--container\">
                    ";
            // line 31
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 31, $this->source); })()), "Submit", [], "any", false, false, false, 31), 'row');
            echo "
                </div>
            ";
            // line 33
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["form"]) || array_key_exists("form", $context) ? $context["form"] : (function () { throw new RuntimeError('Variable "form" does not exist.', 33, $this->source); })()), 'form_end');
            echo "
        </div>
    </section>
     ";
        } else {
            // line 37
            echo "    <section class=\"sectionRefus\">
        <div class=\"sectionRefus--container\">
            <p class=\"sectionRefus--text\">Veuillez vous connecter.</p>
        </div>
    </section>
    ";
        }
        // line 43
        echo "    <script>
    function isChecked(){
        let select = document.getElementById(\"form_contrat\");
        let select_2 = select.options[select.selectedIndex];
        if(select_2.innerHTML === \"CDI\") {
            document.getElementById(\"form_fin_mission\").style.display = \"none\";
            document.getElementById(\"label\").style.display = \"none\";
        } else {
            document.getElementById(\"form_fin_mission\").style.display = \"block\";
            document.getElementById(\"label\").style.display = \"block\";
        }
     }
    </script>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/modifier.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  144 => 43,  136 => 37,  129 => 33,  124 => 31,  118 => 28,  112 => 25,  106 => 22,  100 => 19,  94 => 16,  88 => 13,  82 => 10,  77 => 8,  73 => 6,  71 => 5,  68 => 4,  58 => 3,  35 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block body %}

    {% if app.user %}
    <section class=\"sectionForm\">
        <div class=\"sectionForm--container\">
            {{ form_start(form) }}
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.Title) }}
                </div>
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.contrat) }}
                </div>
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.typeContrat) }}
                </div>
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.fin_mission) }}
                </div>
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.Description) }}
                </div>
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.Adresse) }}
                </div>
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.Code_postal) }}
                </div>
                <div class=\"sectionForm__form--container\">
                    {{ form_row(form.Submit) }}
                </div>
            {{ form_end(form) }}
        </div>
    </section>
     {% else %}
    <section class=\"sectionRefus\">
        <div class=\"sectionRefus--container\">
            <p class=\"sectionRefus--text\">Veuillez vous connecter.</p>
        </div>
    </section>
    {% endif %}
    <script>
    function isChecked(){
        let select = document.getElementById(\"form_contrat\");
        let select_2 = select.options[select.selectedIndex];
        if(select_2.innerHTML === \"CDI\") {
            document.getElementById(\"form_fin_mission\").style.display = \"none\";
            document.getElementById(\"label\").style.display = \"none\";
        } else {
            document.getElementById(\"form_fin_mission\").style.display = \"block\";
            document.getElementById(\"label\").style.display = \"block\";
        }
     }
    </script>

{% endblock %}", "security/modifier.html.twig", "C:\\Users\\Juliettedlc\\Documents\\Ynov\\symfony_blog\\blog\\templates\\security\\modifier.html.twig");
    }
}
