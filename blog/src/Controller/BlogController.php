<?php

namespace App\Controller;

use App\Entity\Contrat;
use App\Entity\Offre;
use App\Entity\Typecontrat;
use App\Repository\OffreRepository;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

class BlogController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {
        return $this->render('blog/index.html.twig', [
            
        ]);
    }
    /**
     * @Route("/article", name="article_list")
     */
    public function list(OffreRepository $ripo): Response
    {
        $offre = $ripo->findAll();


        return $this->render('blog/article.html.twig', [
            'offre' => $offre,
        ]);
    }

    /**
     * @Route("/article/{id}",name="offre.show")
     */
    public function show(Offre $offres, Request $request)
    {
        // dd($request->get('id'));

        return $this->render('blog/offre.html.twig', [
            'offres' => $offres,
        ]);
    }

    /**
     * @Route("/create", name="offers.create")
     */
    public function create(Request $request, EntityManagerInterface $em) {
        
        $offre = new Offre();

        $form = $this->createFormBuilder($offre)
            ->add("Title", TextType::class, [
                "label" => "Titre du job : ",
                "attr" => [
                    "class" => "sectionForm--text",
                    "placeholder" => "Entrez le nom du job",
                ]
            ])
            ->add('contrat', EntityType::class, [
                "label" => "Contrat : ",
                "class" => Contrat::class,
                "attr" => [
                    "class" => "sectionForm--select",
                    "onchange" => "isChecked()"
                ],
            ])
            ->add('typeContrat', EntityType::class, [
                "label" => "Type du contrat : ",
                "class" => TypeContrat::class,
                "attr" => [
                    "class" => "sectionForm--select",
                ],
            ])
           ->add('fin_mission', DateType::class, [
                "label" => "Fin de la mission : ",
                "label_attr" => [
                    "id" => "label"
                ],
                "attr" => [
                    "class" => "sectionForm--date",
                ]
            ])         
            ->add("Description", TextareaType::class, [
                "label" => "Description du job : ",
                "attr" => [
                    "class" => "sectionForm--text"
                ]
            ])
            ->add("Adresse", TextType::class, [
                "label" => "Adresse : ",
                "attr" => [
                    "class" => "sectionForm--text",
                    "placeholder" => "Entrez l'adresse",
                ]
            ])
            ->add("Code_postal", TextType::class, [
                "label" => "Code postal : ",
                "attr" => [
                    "class" => "sectionForm--text",
                    "placeholder" => "Entrez le code postal",
                ]
            ])
            ->add("Submit", SubmitType::class, [
                "attr" => [
                    "class" => "sectionForm--btn"
                ]
            ])
            ->getForm();

        $offre->setDateCreation(new \DateTime());

        $form->handleRequest($request);

        if($form->isSubmitted() && $form->isValid()) {
            $em->persist($offre);
            $em->flush();
            $this->addFlash('success', 'Votre offre a bien été publié, merci !');
            return $this->redirectToRoute('home');
        }

        return $this->render('blog/create.html.twig', [
            'form' => $form->createView()
        ]);

    }
}
