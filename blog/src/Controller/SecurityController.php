<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Contrat;
use App\Entity\Typecontrat;
use App\Entity\Offre;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationUtils;
use App\Repository\OffreRepository;


class SecurityController extends AbstractController
{
    /**
     * @Route("/register", name="register")
     */
    public function register(Request $request, EntityManagerInterface $em, UserPasswordEncoderInterface $passwordEncoder)
    {

        $user = new User();

        $form = $this->createFormBuilder($user)
            ->add("Username", TextType::class, [
                "label" => "Pseudonyme : ",
                "attr" => [
                    "class" => "sectionRegister--text",
                    "placeholder" => "Entrez votre pseudo",
                ]
            ])
            ->add("firstName", TextType::class, [
                "label" => "Prénom : ",
                "attr" => [
                    "class" => "sectionRegister--text",
                    "placeholder" => "Prénom",
                ]
            ])
            ->add("lastName", TextType::class, [
                "label" => "Nom de famille : ",
                "attr" => [
                    "class" => "sectionRegister--text",
                    "placeholder" => "Nom de famille",
                ]
            ])
            ->add("email", EmailType::class, [
                "label" => "E-mail : ",
                "attr" => [
                    "class" => "sectionRegister--text",
                    "placeholder" => "E-mail",
                ]
            ])
            ->add("password", PasswordType::class, [
                "label" => "Mot de passe : ",
                "attr" => [
                    "class" => "sectionRegister--text",
                    "placeholder" => "Mot de passe",
                ]
                //encode password
            ])
            ->add("Submit", SubmitType::class, [
                "attr" => [
                    "class" => "sectionRegister--btn"
                ]
            ])
            ->getForm();

            $form->handleRequest($request);

            if($form->isSubmitted() && $form->isValid()) {
                $hash = $passwordEncoder->encodePassword($user, $user->getPassword()); 
                $user->setPassword($hash);
                $em->persist($user);
                $em->flush();
                $this->addFlash('success', 'Votre compte a bien été créé.');
                return $this->redirectToRoute('home');
            }

        return $this->render('security/index.html.twig', [
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/connexion", name="connexion")
     */
    public function connexion(AuthenticationUtils $authenticationUtils)
    {

        $error = $authenticationUtils->getLastAuthenticationError();
        $lastUsername = $authenticationUtils->getLastUsername();

        return $this->render('security/connexion.html.twig', [
            'last_username' => $lastUsername,
           'error'         => $error,
        ]);
    }

    /**
     * @Route("/tableau", name="tableau")
     */
    public function crud(OffreRepository $ripo): Response
    {
        $offre = $ripo->findAll();

        return $this->render('security/tableau.html.twig', [
            'offre' => $offre,
        ]);
    }

    /**
        * @Route("/modifier/{id}", name="modifier")
    */
    public function modifier(Request $request, EntityManagerInterface $em, Offre $offre, $id): Response
    {

        $offre = $em->getRepository('App\Entity\Offre')->find($id);
        $form = $this->createFormBuilder($offre)
            ->add("Title", TextType::class, [
                "label" => "Titre du job : ",
                "attr" => [
                    "class" => "sectionForm--text",
                    "placeholder" => "Entrez le nom du job",
                ]
            ])
            ->add('contrat', EntityType::class, [
                "label" => "Contrat : ",
                "class" => Contrat::class,
                "attr" => [
                    "class" => "sectionForm--select",
                    "onchange" => "isChecked()"
                ],
            ])
            ->add('typeContrat', EntityType::class, [
                "label" => "Type du contrat : ",
                "class" => TypeContrat::class,
                "attr" => [
                    "class" => "sectionForm--select",
                ],
            ])
           ->add('fin_mission', DateType::class, [
                "label" => "Fin de la mission : ",
                "label_attr" => [
                    "id" => "label"
                ],
                "attr" => [
                    "class" => "sectionForm--date",
                ]
            ])         
            ->add("Description", TextareaType::class, [
                "label" => "Description du job : ",
                "attr" => [
                    "class" => "sectionForm--text"
                ]
            ])
            ->add("Adresse", TextType::class, [
                "label" => "Adresse : ",
                "attr" => [
                    "class" => "sectionForm--text",
                    "placeholder" => "Entrez l'adresse",
                ]
            ])
            ->add("Code_postal", TextType::class, [
                "label" => "Code postal : ",
                "attr" => [
                    "class" => "sectionForm--text",
                    "placeholder" => "Entrez le code postal",
                ]
            ])
            ->add("Submit", SubmitType::class, [
                "attr" => [
                    "class" => "sectionForm--btn"
                ]
            ])
            ->getForm();

            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $offre = $form->getData();
            $em->persist($offre);
            $em->flush();
            $this->addFlash('success', 'Votre offre a bien été modifié, merci !');
            return $this->redirectToRoute('home');
        }
    
        return $this->render("security/modifier.html.twig", [
            "form" => $form->createView(),
        ]);
    }
    /**
        * @Route("/delete/{id}", name="delete")
    */
    public function delete(Request $request, EntityManagerInterface $em, Offre $offre, $id): Response
    {

        $offre = $em->getRepository('App\Entity\Offre')->find($id);
        $em->remove($offre);
        $em->flush();
        $this->addFlash('success', 'Votre offre a bien été supprimé, merci !');
        return $this->redirectToRoute("home");
    }
}
