<?php

namespace App\DataFixtures;

use App\Entity\Contrat;
use App\Entity\Offre;
use App\Entity\Typecontrat;
use App\Repository\ContratRepository;
use App\Repository\OffreRepository;
use App\Repository\TypecontratRepository;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager)
    {
        $faker = Factory::create('fr_FR');

        $contratCDI = new Contrat();
        $contratCDI->setName("CDI");
        $manager->persist($contratCDI);


        $contratCDD = new Contrat();
        $contratCDD->setName("CDD");
        $manager->persist($contratCDD);


        $contratFree = new Contrat();
        $contratFree->setName("Freelance");
        $manager->persist($contratFree);


        $typeContratPart = new Typecontrat();
        $typeContratPart->setName("Temps partiel");
        $manager->persist($typeContratPart);

        $typeContratPlein = new Typecontrat();
        $typeContratPlein->setName("Temps plein");
        $manager->persist($typeContratPlein);

        $contrat = array(
            $contratCDI,
            $contratCDD,
            $contratFree
        );
        $type = array(
            $typeContratPart,
            $typeContratPlein
        );



        for($i=0;$i<10;$i++) {
            $contratkey = array_rand($contrat);
            $typekey = array_rand($type);
            $offre = new Offre;
        $offre->setTitle($faker->jobTitle())
              ->setDescription($faker->realText($maxNbChars = 500))
              ->setAdresse($faker->address())
              ->setCodePostal($faker->postcode())
              ->setDateCreation($faker->dateTimeBetween($startDate='-1 years', $endDate='-2 months'))
              ->setDateUpdate($faker->dateTimeBetween($startDate='-2 months', $endDate='now'))
              ->setContrat($contrat[$contratkey])
              ->setTypecontrat($type[$typekey]);

        if ($contratkey === 1) {
            $offre->setFinMission($faker->dateTimeBetween($startDate='-2 months', $endDate='now'));
        } else if ($contratkey === 2) {
            $offre->setFinMission($faker->dateTimeBetween($startDate='-2 months', $endDate='now'));
        }
        $manager->persist($offre);

        }

        $manager->flush();
    }
}
