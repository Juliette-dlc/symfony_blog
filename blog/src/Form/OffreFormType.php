<?php

namespace App\Form;

use App\Entity\Offre;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\DateType;

class OffreFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('adresse')
            ->add('code_postal')
            ->add('date_creation')
            ->add('date_update')
            ->add('fin_mission', DateType::class, [
                "label" => "Fin de la mission : ",
                "label_attr" => [
                    "id" => "label"
                ],
                "attr" => [
                    "class" => "sectionForm--date",
                ]
            ])  
            ->add('contrat')
            ->add('typecontrat')
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Offre::class,
        ]);
    }
}
