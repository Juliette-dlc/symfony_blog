<?php

namespace App\Entity;

use App\Repository\TypecontratRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=TypecontratRepository::class)
 */
class Typecontrat
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity=Offre::class, mappedBy="typecontrat")
     */
    private $relation;

    public function __construct()
    {
        $this->relation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Offre[]
     */
    public function getRelation(): Collection
    {
        return $this->relation;
    }

    public function addRelation(Offre $relation): self
    {
        if (!$this->relation->contains($relation)) {
            $this->relation[] = $relation;
            $relation->setTypecontrat($this);
        }

        return $this;
    }

    public function removeRelation(Offre $relation): self
    {
        if ($this->relation->removeElement($relation)) {
            // set the owning side to null (unless already changed)
            if ($relation->getTypecontrat() === $this) {
                $relation->setTypecontrat(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return (string) $this->getName();
    }
}
